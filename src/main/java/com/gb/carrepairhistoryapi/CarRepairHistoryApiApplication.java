package com.gb.carrepairhistoryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarRepairHistoryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarRepairHistoryApiApplication.class, args);
	}

}
