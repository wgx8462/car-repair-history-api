package com.gb.carrepairhistoryapi.entity;

import com.gb.carrepairhistoryapi.enums.BrokenPart;
import com.gb.carrepairhistoryapi.enums.CarName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class CarRepairHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private CarName carName;

    @Column(nullable = false)
    private LocalDate problemDate;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BrokenPart brokenPart;

    @Column(length = 40)
    private String repairMemo;

    @Column(nullable = false)
    private Boolean isSelf;

    @Column(length = 15)
    private String repairShop;

    private LocalDate repairDate;

    private Double repairCost;

    @Column(nullable = false)
    private Boolean isCompleted;
}
