package com.gb.carrepairhistoryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum CarName {
    AVANTE("아반떼"),
    FORTE("포르테"),
    SONATA("소나타"),
    K3("K-3"),
    K5("K-5"),
    K7("K-7"),
    GRANGER("그렌저"),
    GENESIS("제네시스");

    private String name;
}
