package com.gb.carrepairhistoryapi.repository;

import com.gb.carrepairhistoryapi.entity.CarRepairHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepairHistoryRepository extends JpaRepository<CarRepairHistory, Long> {
}
