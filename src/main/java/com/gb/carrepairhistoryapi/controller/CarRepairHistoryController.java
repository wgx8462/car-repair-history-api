package com.gb.carrepairhistoryapi.controller;

import com.gb.carrepairhistoryapi.model.CarRepairHistoryItem;
import com.gb.carrepairhistoryapi.model.CarRepairHistoryRequest;
import com.gb.carrepairhistoryapi.model.CarRepairHistoryResponse;
import com.gb.carrepairhistoryapi.service.CarRepairHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carRepair-history")
public class CarRepairHistoryController {
    private final CarRepairHistoryService carRepairHistoryService;

    @PostMapping("/new")
    public String setCarRepairHistory(@RequestBody CarRepairHistoryRequest request) {
        carRepairHistoryService.setCarRepairHistory(request);

        return "Ok";
    }

    @GetMapping("/all")
    public List<CarRepairHistoryItem> getCarRepairHistorys() {
        return carRepairHistoryService.getCarRepairHistorys();
    }

    @GetMapping("/detail/{id}")
    public CarRepairHistoryResponse getCarRepairHistory(@PathVariable long id) {
        return carRepairHistoryService.getCarRepairHistory(id);
    }

}
