package com.gb.carrepairhistoryapi.model;

import com.gb.carrepairhistoryapi.enums.CarName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRepairHistoryItem {
    private Long id;
    private String carNames;
    private LocalDate problemDate;
    private String isSelfName;
    private LocalDate repairDate;
    private String isCompletedName;
}
