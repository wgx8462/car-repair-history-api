package com.gb.carrepairhistoryapi.model;

import com.gb.carrepairhistoryapi.enums.BrokenPart;
import com.gb.carrepairhistoryapi.enums.CarName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRepairHistoryResponse {
    private Long id;
    private String carNames;
    private LocalDate problemDate;
    private String brokenPartName;
    private String repairMemo;
    private String isSelfName;
    private String repairShop;
    private LocalDate repairDate;
    private Double repairCost;
    private String isCompletedName;
}
