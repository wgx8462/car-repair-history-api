package com.gb.carrepairhistoryapi.model;

import com.gb.carrepairhistoryapi.enums.BrokenPart;
import com.gb.carrepairhistoryapi.enums.CarName;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRepairHistoryRequest {
    @Enumerated(value = EnumType.STRING)
    private CarName carName;
    private LocalDate problemDate;
    @Enumerated(value = EnumType.STRING)
    private BrokenPart brokenPart;
    private String repairMemo;
    private Boolean isSelf;
    private String repairShop;
    private LocalDate repairDate;
    private Double repairCost;
    private Boolean isCompleted;
}
