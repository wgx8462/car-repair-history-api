package com.gb.carrepairhistoryapi.service;

import com.gb.carrepairhistoryapi.entity.CarRepairHistory;
import com.gb.carrepairhistoryapi.model.CarRepairHistoryRequest;
import com.gb.carrepairhistoryapi.model.CarRepairHistoryItem;
import com.gb.carrepairhistoryapi.model.CarRepairHistoryResponse;
import com.gb.carrepairhistoryapi.repository.CarRepairHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarRepairHistoryService {
    private final CarRepairHistoryRepository carRepairHistoryRepository;

    public void setCarRepairHistory(CarRepairHistoryRequest request) {
        CarRepairHistory addData = new CarRepairHistory();
        addData.setCarName(request.getCarName());
        addData.setProblemDate(request.getProblemDate());
        addData.setBrokenPart(request.getBrokenPart());
        addData.setRepairMemo(request.getRepairMemo());
        addData.setIsSelf(request.getIsSelf());
        addData.setRepairShop(request.getRepairShop());
        addData.setRepairDate(request.getRepairDate());
        addData.setRepairCost(request.getRepairCost());
        addData.setIsCompleted(request.getIsCompleted());

        carRepairHistoryRepository.save(addData);
    }

    public List<CarRepairHistoryItem> getCarRepairHistorys() {
        List<CarRepairHistory> originList = carRepairHistoryRepository.findAll();

        List<CarRepairHistoryItem> result = new LinkedList<>();

        for (CarRepairHistory carRepairHistory : originList) {
            CarRepairHistoryItem addItem = new CarRepairHistoryItem();
            addItem.setId(carRepairHistory.getId());
            addItem.setCarNames(carRepairHistory.getCarName().getName());
            addItem.setProblemDate(carRepairHistory.getProblemDate());
            addItem.setIsSelfName(carRepairHistory.getIsSelf() ? "직접 수리" : "카센타 수리");
            addItem.setRepairDate(carRepairHistory.getRepairDate());
            addItem.setIsCompletedName(carRepairHistory.getIsCompleted() ? "수리 완료" : "수리중");

            result.add(addItem);
        }
        return result;
    }

    public CarRepairHistoryResponse getCarRepairHistory(long id) {
        CarRepairHistory originData = carRepairHistoryRepository.findById(id).orElseThrow();

        CarRepairHistoryResponse response = new CarRepairHistoryResponse();
        response.setId(originData.getId());
        response.setCarNames(originData.getCarName().getName());
        response.setProblemDate(originData.getProblemDate());
        response.setBrokenPartName(originData.getBrokenPart().getName());
        response.setRepairMemo(originData.getRepairMemo());
        response.setIsSelfName(originData.getIsSelf() ? "직접 수리" : "카센타 수리");
        response.setRepairShop(originData.getRepairShop());
        response.setRepairDate(originData.getRepairDate());
        response.setRepairCost(originData.getRepairCost());
        response.setIsCompletedName(originData.getIsCompleted() ? "수리 완료" : "수리중");

        return response;
    }
}
